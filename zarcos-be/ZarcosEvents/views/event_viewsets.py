from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from ZarcosEvents.models.event import Event
from ZarcosEvents.serializers.event_serializers import EventSerializer

class EventViewSet(viewsets.ModelViewSet):
    Model = Event
    serializer_class = EventSerializer

    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.filter(state=True)
        return self.get_serializer().Meta.model.objects.filter(id=pk, state=True).first()

    def list(self, request):
        for key, value in request.__dict__.items():
            print(key, '==', value)
        event_serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(event_serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        # send information to serializer
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'Evento creado correctamente.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if self.get_queryset(pk):
            # send information to serializer referencing the instance
            event_serializer = self.serializer_class(self.get_queryset(pk), data=request.data)
            if event_serializer.is_valid():
                event_serializer.save()
                return Response(event_serializer.data, status=status.HTTP_200_OK)
            return Response(event_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self,request,pk=None):
        event = self.get_queryset().filter(id=pk).first() # get instance
        if event:
            event.state = False
            event.save()
            return Response({'message':'Evento eliminado correctamente.'}, status=status.HTTP_200_OK)
        return Response({'error':'No existe un Evento con esta información.'}, status=status.HTTP_400_BAD_REQUEST)
