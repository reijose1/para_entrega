from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
#from ZarcosEvents.abstract.apiview import GeneralListApiView
from ZarcosEvents.models.eventType import EventType
from ZarcosEvents.models.category import Category
from ZarcosEvents.models.place import Place
from ZarcosEvents.serializers.general_serializers import EventTypeSerializer,PlaceSerializer,CategorySerializer

class EventTypeViewSet(viewsets.GenericViewSet):
    model = EventType
    serializer_class = EventTypeSerializer

    def get_queryset(self):
        return self.get_serializer().Meta.model.objects.filter(state=True)

    def list(self, request):
        data = self.get_queryset()
        data = self.get_serializer(data, many=True)
        return Response(data.data)
    
    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Tipo de Evento registrado exitosamente.'}, status=status.HTTP_201_CREATED)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if self.get_object().exists():
            serializer = self.serializer_class(instance=self.get_object().get(), data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'message':'Tipo de Evento actualizado correctamente.'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if self.get_object().exists():
            self.get_object().get().delete()
            return Response({'message':'Tipo de Evento eliminado correctamente.'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':'Tipo de Evento no encontrado.'}, status=status.HTTP_400_BAD_REQUEST)


class PlaceViewSet(viewsets.GenericViewSet):
    model = Place
    serializer_class = PlaceSerializer

    def get_queryset(self):
        return self.get_serializer().Meta.model.objects.filter(state=True)

    def list(self, request):
        data = self.get_queryset()
        data = self.get_serializer(data,many = True)
        return Response(data.data)
    
    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Lugar registrado exitosamente.'}, status=status.HTTP_201_CREATED)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if self.get_object().exists():
            serializer = self.serializer_class(instance=self.get_object().get(), data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'message':'Lugar actualizado correctamente.'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if self.get_object().exists():
            self.get_object().get().delete()
            return Response({'message':'Lugar eliminado correctamente.'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':'Lugar no identificado.'}, status=status.HTTP_400_BAD_REQUEST)

class CategoryViewSet(viewsets.GenericViewSet):
    model = Category
    serializer_class = CategorySerializer

    def get_queryset(self):
        return self.get_serializer().Meta.model.objects.filter(state=True)

    def get_object(self):
        return self.get_serializer().Meta.model.objects.filter(id=self.kwargs['pk'], state=True)

    def list(self, request):
        data = self.get_queryset()
        data = self.get_serializer(data, many=True)
        return Response(data.data)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Categoría registrada correctamente!'}, status=status.HTTP_201_CREATED)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if self.get_object().exists():
            serializer = self.serializer_class(instance=self.get_object().get(), data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'message':'Categoría actualizada correctamente!'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if self.get_object().exists():
            self.get_object().get().delete()
            return Response({'message':'Categoría eliminada correctamente!'}, status=status.HTTP_200_OK)
        return Response({'message':'', 'error':'Categoría no encontrada!'}, status=status.HTTP_400_BAD_REQUEST)
