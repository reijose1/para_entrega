from django.contrib import admin
from ZarcosEvents.models.category import Category
from ZarcosEvents.models.event import Event
from ZarcosEvents.models.eventType import EventType
from ZarcosEvents.models.place import Place
from ZarcosEvents.models.user import User

# Register your models here.
class EventTypeAdmin(admin.ModelAdmin):
    list_display = ('id','name')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','name')

admin.site.register(EventType,EventTypeAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(Place)
admin.site.register(Event)
admin.site.register(User)
