from rest_framework import serializers
from ZarcosEvents.models.category import Category
from ZarcosEvents.models.eventType import EventType
from ZarcosEvents.models.place import Place

class EventTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventType
        exclude = ('state','created_date','modified_date','deleted_date')

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        exclude = ('state','created_date','modified_date','deleted_date')

class PlaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Place
        exclude = ('state','created_date','modified_date','deleted_date')