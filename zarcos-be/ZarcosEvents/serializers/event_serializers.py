from rest_framework import serializers
from ZarcosEvents.models.event import Event
from ZarcosEvents.serializers.general_serializers import *

class EventSerializer(serializers.ModelSerializer):
    event_type = serializers.StringRelatedField()
    place = serializers.StringRelatedField()
    category = serializers.StringRelatedField()

    class Meta:
        model = Event
        exclude = ('state','created_date','modified_date','deleted_date')

'''
    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'image': instance.image if instance.image != '' else '',
            'event_type': instance.event_type.name if instance.event_type is not None else '',
            'category': instance.category.name if instance.category is not None else '',
            'place' : instance.place.address + " - " + instance.place.city if instance.place is not None else '',
            'date' : instance.date,
            'price' : instance.price,
            'capacity': instance.capacity
        }
    '''