from rest_framework.routers import DefaultRouter
from ZarcosEvents.views.event_viewsets import EventViewSet
from ZarcosEvents.views.general_views import EventTypeViewSet, PlaceViewSet, CategoryViewSet
from ZarcosEvents.views.event_viewsets import EventViewSet

router = DefaultRouter()

router.register(r'events',EventViewSet,basename = 'events')
router.register(r'event_type',EventTypeViewSet,basename = 'event_type')
router.register(r'place',PlaceViewSet,basename = 'place')
router.register(r'category',CategoryViewSet,basename = 'category')

urlpatterns = router.urls