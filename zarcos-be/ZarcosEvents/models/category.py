from django.db import models
from simple_history.models import HistoricalRecords
from ZarcosEvents.models.abstract import AbstractModel

class Category(AbstractModel):
    """Model definition for Category."""

    # TODO: Define fields here
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=50,unique = True,null = False,blank = False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Category."""

        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'
    def __str__(self):
        """Unicode representation of Category."""
        return self.name
