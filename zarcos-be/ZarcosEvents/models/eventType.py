from django.db import models
from simple_history.models import HistoricalRecords
from ZarcosEvents.models.abstract import AbstractModel
# Create your models here.

class EventType(AbstractModel):
    """Model definition for EventType."""

    # TODO: Define fields here
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=50,blank = False,null = False,unique = True)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for EventType."""

        verbose_name = 'Tipo de Evento'
        verbose_name_plural = 'Tipos de Evento'

    def __str__(self):
        """Unicode representation of EventType."""
        return self.name
