from django.db import models
from datetime import timedelta
from simple_history.models import HistoricalRecords
from ZarcosEvents.models.abstract import AbstractModel
from ZarcosEvents.models.eventType import EventType
from ZarcosEvents.models.category import Category
from ZarcosEvents.models.place import Place

class Event(AbstractModel):
    """Model definition for Event."""

    # TODO: Define fields here
    id = models.AutoField(primary_key=True)
    name = models.CharField('Nombre del Evento', max_length=150, unique = True,blank = False,null = False)
    description = models.TextField('Descripción',blank = False,null = False)
    image = models.ImageField('Imagen', upload_to='products/',blank = True,null = True)
    event_type = models.ForeignKey(EventType, on_delete=models.CASCADE,verbose_name = 'Tipo de Evento', null = True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,verbose_name = 'Categoria', null = True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE,verbose_name = 'Lugar', null = True)
    date = models.DateTimeField('Fecha y hora',)
    price = models.DecimalField('Precio', max_digits=9, decimal_places=2, default=0.0)
    capacity = models.IntegerField('Capacidad', default=0, null=True, blank=True)
    is_active = models.BooleanField('está activo?', default=True)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Event."""

        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

    def __str__(self):
        """Unicode representation of Event."""
        return self.name
