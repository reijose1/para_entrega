from django.db import models

# Create your models here.
class AbstractModel(models.Model):
    """Model definition for AbstractModel."""

    # TODO: Define fields here
    id = models.AutoField(primary_key = True)
    state = models.BooleanField('Estado',default = True)
    created_date = models.DateField('Creado', auto_now=False, auto_now_add=True)
    modified_date = models.DateField('Modificado', auto_now=True, auto_now_add=False)
    deleted_date = models.DateField('Eliminacido', auto_now=True, auto_now_add=False)

    class Meta:
        """Meta definition for AbstractModel."""
        abstract = True
        verbose_name = 'Modelo Abstracto'
        verbose_name_plural = 'Modelos Abstractos'
