from django.db import models
from simple_history.models import HistoricalRecords
from ZarcosEvents.models.abstract import AbstractModel

class Place(AbstractModel):
    """Model definition for Indicator."""

    # TODO: Define fields here
    id = models.AutoField(primary_key=True)
    city = models.CharField('city', max_length=40)
    address = models.CharField('address', max_length = 100)
    name_place = models.CharField('name', max_length = 100, blank=True, null=False)
    complement = models.CharField('Complement', max_length = 256, blank=True, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Place."""

        verbose_name = 'Lugar'
        verbose_name_plural = 'Lugares'

    def __str__(self):
        """Unicode representation of Place."""
        return f' {self.address} - {self.city}%' 

