from django.urls import path
#from ZarcosEvents.events.views.general_views import EventTypeListAPIView,PlaceListAPIView,CategoryListAPIView
#from ZarcosEvents.events.views.event_viewsets import EventListCreateAPIView,EventRetrieveUpdateDestroyAPIView
from ZarcosEvents.views.userView import user_api_view, user_detail_api_view

urlpatterns = [
    #path('event_type/',EventTypeListAPIView.as_view(), name = 'event_type'),
    #path('place/',PlaceListAPIView.as_view(), name = 'place'),
    #path('category/',CategoryListAPIView.as_view(), name = 'category'),
   #path('event/',EventListCreateAPIView.as_view(), name = 'events'),
    #path('event/<int:pk>',EventRetrieveUpdateDestroyAPIView.as_view(), name = 'event'),
    path('usuario/',user_api_view, name = 'usuario_api'),
    path('usuario/<int:pk>/',user_detail_api_view, name = 'usuario_detail_api_view')
]