from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    "*",
    '0.0.0.0'
    'localhost'
    'http://zarcos-webapp-be.herokuapp.com',
    'https://zarcos-webapp-be.herokuapp.com',
    'http://127.0.0.1:8000'
    'https://127.0.0.1:8000'
    'http://127.0.0.1:8080'
    'https://127.0.0.1:8080'
]
CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_CREDENTIALS = True

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'dfk84j71hirlje',
    'USER': 'qminmvqkmvdwdv',
    'PASSWORD': '6adaefdc470e65866cddb38dde572a3033421059e5f01a85e98e501c274f839a',
    'HOST': 'ec2-54-209-187-69.compute-1.amazonaws.com',
    'PORT': '5432',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
import django_heroku
django_heroku.settings(locals())