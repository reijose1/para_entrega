from django.contrib import admin
from django.urls import path,include
from rest_framework import permissions
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from ZarcosEvents.views.loginView import Login,Logout


urlpatterns = [
   path('admin/', admin.site.urls),
   path('logout/', Logout.as_view(), name = 'logout'),
   path('login/',Login.as_view(), name = 'login'),
   path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
   path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
   path('usuario/',include('ZarcosEvents.urls')),
   path('events/',include('ZarcosEvents.routers')),
]
