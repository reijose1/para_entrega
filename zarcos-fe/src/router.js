import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import About from './components/About.vue'
import listCategory from './components/generalList/listCategory.vue'
import listEventType from './components/generalList/listEventType.vue'
import listPlace from './components/generalList/listPlace.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/logIn',
    name: 'logIn',
    component: LogIn
  },
  {
    path: '/user/about',
    name: 'about',
    component: About
  },
  {
    path: '/category/listCategory',
    name: 'listCategory',
    component: listCategory
  },
  {
    path: '/place/listPlace',
    name: 'listPlace',
    component: listPlace
  },
  {
    path: '/eventType/listEventType',
    name: 'listEventType',
    component: listEventType
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
